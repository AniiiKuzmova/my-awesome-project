import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import {
  Drawer,
  Box,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Divider,
  Menu,
  MenuItem,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import AssignmentIcon from "@mui/icons-material/Assignment";
import MenuIcon from "@mui/icons-material/Menu";
import CakeIcon from "@mui/icons-material/Cake";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import LayoutStyles from "../../styles/LayoutStyles";
import { toggleMode } from "../../slices/theme";
import { logoutAction } from "../../slices/auth";
import { AccountCircle } from "@mui/icons-material";

const CustomeDrawer = () => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const classes = LayoutStyles();
  const theme = useTheme();
  const links = [
    {
      text: "Tasks",
      icon: <AssignmentIcon />,
      href: "/tasks",
    },
    {
      text: "Birthdays",
      icon: <CakeIcon />,
      href: "/birthdays",
    },
  ];
  const dispatch = useDispatch();

  const renderLinks = (links) => {
    return (
      <List>
        {links.map((item) => {
          return (
            <ListItem
              button
              key={item.text}
              component={RouterLink}
              to={item.href}
            >
              <ListItemIcon> {item.icon}</ListItemIcon>
              <ListItemText>{item.text}</ListItemText>
            </ListItem>
          );
        })}
      </List>
    );
  };

  return (
    <Box>
      <AppBar position="fixed" open={true}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={() => setIsDrawerOpen(true)}
            edge="start"
            sx={{ mr: 2, ...(isDrawerOpen && { display: "none" }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
            Iskam da qm duner
          </Typography>
          <div>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={() => setIsMenuOpen(true)}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={isMenuOpen}
              onClose={() => setIsMenuOpen(false)}
            >
              <MenuItem
                component={RouterLink}
                to="/profile"
                onClose={() => setIsMenuOpen(false)}
              >
                Profile
              </MenuItem>
              <MenuItem
                component={RouterLink}
                to="/account-settings"
                onClose={() => setIsMenuOpen(false)}
              >
                My account
              </MenuItem>
              <MenuItem
                onClick={() => dispatch(logoutAction())}
                onClose={() => setIsMenuOpen(false)}
              >
                Logout
              </MenuItem>
            </Menu>
          </div>
          <IconButton
            onClick={() => dispatch(toggleMode())}
            className={classes.button}
          >
            {theme.palette.mode === "dark" ? (
              <DarkModeIcon />
            ) : (
              <Brightness7Icon />
            )}
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="left"
        open={isDrawerOpen}
        onClose={() => setIsDrawerOpen(false)}
      >
        {renderLinks(links)}
        <Divider />
      </Drawer>
    </Box>
  );
};

export default CustomeDrawer;
