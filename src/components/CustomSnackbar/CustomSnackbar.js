import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Snackbar, Alert } from "@mui/material";
import { close } from "../../slices/snackbar";

const CustomSnackbar = () => {
  const { open, type, message } = useSelector((state) => state.snackbar);
  const dispatch = useDispatch();

  return (
    <Snackbar open={open} autoHideDuration={6000}>
      <Alert
        onClose={() => dispatch(close())}
        severity={type}
        sx={{ width: "100%" }}
      >
        {message}
      </Alert>
    </Snackbar>
  );
};

export default CustomSnackbar;
