import React from 'react'
import CustomeDrawer from '../Drawer/Drawer'
import LayoutStyles from '../../styles/LayoutStyles'

const Layout = ({children}) => {
  const classes = LayoutStyles();

    return (
        <div className={classes.root}>
        <CustomeDrawer/>
        {children}
      </div>
    )
}

export default Layout;
