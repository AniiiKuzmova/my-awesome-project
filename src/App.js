import React from "react";
import { useSelector } from "react-redux";
import { Router, Switch } from "react-router-dom";
import { ThemeProvider } from "@mui/material";
import Reset from "./styles/Reset";
//routes
import GuestRoute from "./routes/GuestRoute/GuestRoute";
import AuthoredRoute from "./routes/GuestRoute/AuthoredRoute";
//pages
import LandingPage from "./pages/Landing/LandingPage";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Dashboard from "./pages/Dashboard/Dashboard";
//components
import CustomSnackbar from "./components/CustomSnackbar/CustomSnackbar";
// import theme from "./styles/theme";
import { history } from "./utils/history";
import darkTheme from "./styles/themes/darkTheme";
import lightTheme from "./styles/themes/lightTheme";

function App() {
  const mode = useSelector((state) => state.theme.mode);

  return (
    <ThemeProvider theme={mode === "light" ? lightTheme : darkTheme}>
      <Reset />
      <Router history={history}>
        <CustomSnackbar />
        <Switch>
          <GuestRoute exact path="/" component={LandingPage} />
          <GuestRoute exact path="/login" component={Login} />
          <GuestRoute exact path="/register" component={Register} />
          <AuthoredRoute exact path="/dashboard" component={Dashboard} />
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
