import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import authServices from "../services/auth.service";
import { history } from "../utils/history";
import { success } from "./snackbar";
import {
  getLocalStorageResource,
  setLocalStorageResource,
} from "../utils/localStorageUtils";

const user = getLocalStorageResource("user");

const registerAction = createAsyncThunk(
  "auth/register",
  async (data, { dispatch, rejectWithValue }) => {
    try {
      const response = await authServices.register(data);
      history.push("/login");
      dispatch(success("Successfully registered."));
      return response.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

const loginAction = createAsyncThunk(
  "auth/login",
  async (data, { rejectWithValue }) => {
    try {
      const response = await authServices.login(data);
      setLocalStorageResource("user", response.data.user);
      setLocalStorageResource("accessToken", response.data.accessToken);
      history.push("/dashboard");
      return response.data.user;
    } catch (error) {
      return rejectWithValue("Invalid creditentials.");
    }
  }
);

const logoutAction = createAsyncThunk("auth/logout", async () => {
  await authServices.logout();
  history.push("/");
});

//initial state variable
const initialStateValues = {
  loading: false,
  errorMessage: null,
  user: user || null,
};

const authSlice = createSlice({
  name: "users",
  initialState: initialStateValues,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(registerAction.pending, (state) => {
        state.loading = true;
        state.errorMessage = null;
      })
      .addCase(registerAction.fulfilled, (state) => {
        state.loading = false;
      })
      .addCase(registerAction.rejected, (state, action) => {
        state.loading = false;
        state.errorMessage = action.payload;
      })
      .addCase(loginAction.pending, (state) => {
        state.loading = true;
        state.errorMessage = null;
      })
      .addCase(loginAction.fulfilled, (state, action) => {
        state.loading = false;
        state.user = action.payload;
      })
      .addCase(loginAction.rejected, (state, action) => {
        state.loading = false;
        state.errorMessage = action.payload;
      })
      .addCase(logoutAction.fulfilled, (state) => {
        state.user = null;
      });
  },
});

export default authSlice.reducer;
export { registerAction, loginAction, logoutAction };
