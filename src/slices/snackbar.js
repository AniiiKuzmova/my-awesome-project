import { createSlice } from "@reduxjs/toolkit";

const initialStateValues = {
  open: false,
  message: "",
  type: "",
};

const snackbarReducer = createSlice({
  name: "snackbar",
  initialState: initialStateValues,
  reducers: {
    success: (state, action) => {
      state.open = true;
      state.message = action.payload;
      state.type = "success";
    },
    warning: (state, action) => {
      state.open = true;
      state.message = action.payload;
      state.type = "warning";
    },
    error: (state, action) => {
      state.open = true;
      state.message = action.payload;
      state.type = "error";
    },
    close: (state) => {
      state.open = false;
      state.message = "";
    },
  },
});

export default snackbarReducer.reducer;
export const { success, error, warning, close } = snackbarReducer.actions;
