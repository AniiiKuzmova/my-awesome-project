import { createSlice } from "@reduxjs/toolkit";
import {
  getLocalStorageResource,
  setLocalStorageResource,
} from "../utils/localStorageUtils";

let themeMode = getLocalStorageResource("themeMode");

if (!themeMode) {
  setLocalStorageResource("themeMode", "light");
  themeMode = "light";
}

const initialStateValue = {
  mode: themeMode,
};

const themeSlice = createSlice({
  name: "theme",
  initialState: initialStateValue,
  reducers: {
    toggleMode: (state) => {
      const selectedMode = state.mode === "light" ? "dark" : "light";
      state.mode = selectedMode;
      setLocalStorageResource("themeMode", selectedMode);
    },
  },
});

export default themeSlice.reducer;
export const { toggleMode } = themeSlice.actions;
