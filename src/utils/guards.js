import { getLocalStorageResource } from "./localStorageUtils";

export const isAuthored = () => {
  const user = getLocalStorageResource("user");
  if (user) {
    return user;
  }
  return false;
};
