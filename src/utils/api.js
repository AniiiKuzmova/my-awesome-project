import axios from "axios";
import { history } from "./history";
import { getLocalStorageResource } from "./localStorageUtils";
import store from "../index";
import { logoutAction } from "../slices/auth";

const api = axios.create({
  baseURL: "http://localhost:8000",
});

api.interceptors.request.use(
  (req) => {
    const token = getLocalStorageResource("accessToken");
    if (token) {
      req.headers.authorization = `Bearer ${token}`;
    }
    return req;
  },
  (error) => {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(undefined, (error) => {
  if (error.response.status === 403 || error.response.status === 401) {
    store.dispatch(logoutAction());
  }
  return Promise.reject(error);
});

export default api;
