const validations = {
  name: {
    required: "Name is required.",
  },
  email: {
    required: "Email is required.",
    pattern: {
      value: /\S+@\S+\.\S+/,
      message: "Incorect email format.",
    },
  },
  password: {
    required: "Password is required.",
    minLength: { value: 6, message: "Password is too short." },
  },
  confirmPassword: {
    required: "Password is required.",
  },
};

export default validations;
