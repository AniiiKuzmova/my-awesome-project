const setLocalStorageResource = (name, resource) => {
  if (name && resource) {
    localStorage.setItem(name, JSON.stringify(resource));
  }
};

const getLocalStorageResource = (key) => {
  return JSON.parse(localStorage.getItem(key)) || "";
};

export { getLocalStorageResource, setLocalStorageResource };
