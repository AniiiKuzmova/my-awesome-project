import { makeStyles } from "@mui/styles";

const landingStyles = makeStyles({
  button: {
    display: "inline-block",
    marginRight: "1em",
    marginLeft: "1em",
  },
  textContent: {
    marginBotton: "1.2em",
  },
});

export default landingStyles;
