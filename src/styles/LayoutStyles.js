import { makeStyles } from "@mui/styles";

const LayoutStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  button: {
    width: "fit-content",
  },
}));

export default LayoutStyles;
