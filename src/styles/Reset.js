import React from "react";
import { CssBaseline } from "@mui/material";

const Reset = () => {
  return (
    <div>
      <CssBaseline />
    </div>
  );
};

export default Reset;
