import { createTheme } from "@mui/material";

const lightTheme = createTheme({
  typography: {
    fontFamily: ["Montserrat", "Arial"].join(","),
  },
  palette: {
    mode: "light",
    divider: "rgba(0, 0, 0, 0.12)",
    background: {
      default: "#fff",
      paper: "#fff",
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.6)",
    },
  },
});

export default lightTheme;
