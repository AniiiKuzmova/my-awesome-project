import { createTheme } from "@mui/material";

const darkTheme = createTheme({
  typography: {
    fontFamily: ["Montserrat", "Arial"].join(","),
  },
  palette: {
    mode: "dark",
    divider: "rgba(255, 255, 255, 0.12)",
    background: {
      default: "#121212",
      paper: "#121212",
    },
    text: {
      primary: "#fff",
      secondary: "rgba(255, 255, 255, 0.7)",
    },
  },
});

export default darkTheme;
