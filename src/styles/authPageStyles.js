import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "block",
    width: "90%",
    margin: "5em auto 0 auto",
    padding: "1em 0",
    boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px",
    [theme.breakpoints.up("md")]: {
      width: "40%",
    },
  },
  inputFieldContainer: {
    marginBottom: "1.5em",
  },
}));

export default useStyles;
