import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Container,
  Box,
  TextField,
  Typography,
  InputAdornment,
  IconButton,
  FormLabel,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { useForm } from "react-hook-form";
import { loginAction } from "../../slices/auth";
import useStyles from "../../styles/authPageStyles";

const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const { loading, errorMessage } = useSelector((state) => state.auth);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const classes = useStyles();
  const dispatch = useDispatch();

  const submitForm = (data) => dispatch(loginAction(data));

  return (
    <Container>
      <Box
        className={classes.form}
        component="form"
        noValidate
        autoComplete="off"
      >
        <Container>
          <Typography
            component="h2"
            variant="h3"
            align="center"
            gutterBottom
            color="textSecondary"
          >
            Login
          </Typography>
          <div className={classes.inputFieldContainer}>
            <TextField
              id="outlined-email-input"
              label="Email"
              type="email"
              autoComplete="current-email"
              fullWidth
              error={errors.email ? true : false}
              helperText={errors.email?.message}
              {...register("email", {
                required: "Email is required.",
                pattern: {
                  value: /\S+@\S+\.\S+/,
                  message: "Incorect email format.",
                },
              })}
            />
          </div>
          <div className={classes.inputFieldContainer}>
            <TextField
              id="outlined-password-input"
              label="Password"
              type={showPassword ? "text" : "password"}
              autoComplete="current-password"
              fullWidth
              error={errors.password ? true : false}
              helperText={errors.password?.message}
              {...register("password", {
                required: "Password is required.",
              })}
              InputProps={{
                // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword(!showPassword)}
                      onMouseDown={() => setShowPassword(!showPassword)}
                    >
                      {showPassword ? (
                        <VisibilityIcon />
                      ) : (
                        <VisibilityOffIcon />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <LoadingButton
            onClick={handleSubmit(submitForm)}
            loading={loading}
            loadingIndicator="Loading..."
            variant="outlined"
          >
            Login
          </LoadingButton>
          {errorMessage && (
            <FormLabel error={true} component="p">
              {errorMessage}
            </FormLabel>
          )}
        </Container>
      </Box>
    </Container>
  );
};

export default Login;
