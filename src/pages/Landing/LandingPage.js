import React from "react";
import { Container, Typography, Button } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import landingStyles from "../../styles/buttonStyles";

const LandingPage = () => {
  const classes = landingStyles();

  return (
    <Container maxWidth="md">
      <Typography align="center" element="h1" variant="h2" gutterBottom>
        Hello
      </Typography>
      <Typography
        className={classes.textContent}
        align="center"
        element="p"
        variant="p"
        color="textSecondary"
      >
        Living valley had silent eat merits esteem bed. In last an or went wise
        as left. Visited civilly am demesne so colonel he calling. In last an or
        went wise.
      </Typography>
      <Container>
        <Button
          className={classes.button}
          component={RouterLink}
          to="/login"
          variant="contained"
          href="#contained-buttons"
        >
          Login
        </Button>
        <Button
          className={classes.button}
          component={RouterLink}
          to="/register"
          variant="outlined"
          href="#outlined-buttons"
        >
          Register
        </Button>
      </Container>
    </Container>
  );
};

export default LandingPage;
