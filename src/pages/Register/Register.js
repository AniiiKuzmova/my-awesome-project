import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Container,
  Box,
  TextField,
  Typography,
  InputAdornment,
  IconButton,
  FormLabel,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { useForm } from "react-hook-form";
import { registerAction } from "../../slices/auth";
import useStyles from "../../styles/authPageStyles";
import validations from "../../utils/validations/auth.validations";

const Register = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const loading = useSelector((state) => state.auth.loading);
  const errorMessage = useSelector((state) => state.auth.errorMessage);
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const classes = useStyles();
  const dispatch = useDispatch();

  const submitForm = (data) => {
    const { name, email, password } = data;
    dispatch(registerAction({ name, email, password }));
  };

  return (
    <Container>
      <Box
        className={classes.form}
        component="form"
        noValidate
        autoComplete="off"
      >
        <Container>
          <Typography
            component="h2"
            variant="h3"
            align="center"
            gutterBottom
            color="textSecondary"
          >
            Register
          </Typography>
          <div className={classes.inputFieldContainer}>
            <TextField
              id="outlined-text-input"
              label="Name"
              type="text"
              fullWidth
              error={errors.email ? true : false}
              helperText={errors.name?.message}
              {...register("name", validations.name)}
            />
          </div>
          <div className={classes.inputFieldContainer}>
            <TextField
              id="outlined-email-input"
              label="Email"
              type="email"
              autoComplete="current-email"
              fullWidth
              error={errors.email ? true : false}
              helperText={errors.email?.message}
              {...register("email", validations.email)}
            />
          </div>
          <div className={classes.inputFieldContainer}>
            <TextField
              id="outlined-password-input"
              label="Password"
              type={showPassword ? "text" : "password"}
              autoComplete="current-password"
              fullWidth
              error={errors.password ? true : false}
              helperText={errors.password?.message}
              {...register("password", validations.password)}
              InputProps={{
                // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword(!showPassword)}
                      onMouseDown={() => setShowPassword(!showPassword)}
                    >
                      {showPassword ? (
                        <VisibilityIcon />
                      ) : (
                        <VisibilityOffIcon />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className={classes.inputFieldContainer}>
            <TextField
              id="outlined-confirmPassword-input"
              label="Confirm Password"
              type={showConfirmPassword ? "text" : "password"}
              autoComplete="current-password"
              fullWidth
              error={errors.confirmPassword ? true : false}
              helperText={errors.confirmPassword?.message}
              {...register("confirmPassword", {
               ...validations.confirmPassword,
                validate: (value) =>
                  value === watch("password") || "Passwords don't match.",
              })}
              InputProps={{
                // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() =>
                        setShowConfirmPassword(!showConfirmPassword)
                      }
                      onMouseDown={() =>
                        setShowConfirmPassword(!showConfirmPassword)
                      }
                    >
                      {showConfirmPassword ? (
                        <VisibilityIcon />
                      ) : (
                        <VisibilityOffIcon />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <LoadingButton
            onClick={handleSubmit(submitForm)}
            loading={loading}
            loadingIndicator="Loading..."
            variant="outlined"
          >
            Register
          </LoadingButton>
          {errorMessage && (
            <FormLabel error={true} component="p">
              {errorMessage}
            </FormLabel>
          )}
        </Container>
      </Box>
    </Container>
  );
};

export default Register;
