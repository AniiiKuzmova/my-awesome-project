import api from "../utils/api";

const register = (data) => {
  return api.post("/register", data);
};

const login = (data) => {
  return api.post("/signin", data);
};

const logout = () => {
  localStorage.removeItem("user");
  localStorage.removeItem("accessToken");
};

const authServices = {
  register,
  login,
  logout,
};

export default authServices;
